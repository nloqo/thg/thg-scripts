from pyleco.directors.data_logger_director import DataLoggerDirector


name = "Spectroscopy"


configuration = {
    "trigger_type": "variable",
    "trigger_timeout": 0.05,
    "trigger_variable": "Picoscope",
    "variables": [
        "time",
        "APD",
        "APD-cw",
        "PMT",
        "PMT-cw",
        "MYRES.HeinzingerHV.highVoltage",
        "Nova2",
        "idler2",
        "idler2_E",
        "precisionPressure",
        "HCl_pressure",
        "signal_wl",
        "signal_P",
        "seedWavelengthCalculated",
        "pump_wl",
        "Picoscope",
        "highVoltage",
    ],
    "units": {
        "time": "s",
        "APD": "Wb",
        "APD-cw": "V",
        "PMT": "Wb",
        "PMT-cw": "V",
        "MYRES.HeinzingerHV.highVoltage": "V",
        "Nova2": "J",
        "idler2": "Wb",
        "idler2_E": "J",
        "precisionPressure": "mbar",
        "HCl_pressure": "mbar",
        "signal_wl": "nm",
        "signal_P": "µW",
        "seedWavelengthCalculated": "nm",
        "pump_wl": "nm",
        "highVoltage": "V",
    },
    "valuing_mode": "last",
    "value_repeating": False,
}


gui_configuration = {"header": "", "unitsText": "", "autoSave": False, "pause": False}


plot_configuration = [
    {
        "name": "Plot 0",
        "type": "SinglePlotWidget",
        "x_key": "index",
        "autoCut": 200,
        "ly": False,
        "lg": 3533.120178675714,
        "vls": False,
        "evaluation": True,
        "y_key": "idler_wl",
        "dots": False,
        "lmm": False,
        "mm": False,
    },
    {
        "name": "Plot 2",
        "type": "SinglePlotWidget",
        "x_key": "index",
        "autoCut": 200,
        "ly": False,
        "lg": False,
        "vls": False,
        "evaluation": True,
        "y_key": "PMT",
        "dots": False,
        "lmm": False,
        "mm": False,
    },
    {
        "name": "Plot 3",
        "type": "SinglePlotWidget",
        "x_key": "idler_wl",
        "autoCut": 2000,
        "ly": False,
        "lg": False,
        "vls": (3533.103910832846, 3533.122162478857),
        "evaluation": False,
        "y_key": "PMT",
        "dots": True,
        "lmm": False,
        "mm": False,
    },
    {
        "name": "Plot 4",
        "type": "MultiPlotWidget",
        "x_key": "idler_wl",
        "autoCut": 2000,
        "ly": False,
        "lg": False,
        "vls": False,
        "evaluation": False,
        "lines": {
            "time": "",
            "APD": "d",
            "APD-cw": "",
            "PMT": "",
            "PMT-cw": "",
            "MYRES.HeinzingerHV.highVoltage": "",
            "Nova2": "",
            "idler2": "",
            "idler2_E": "2,b",
            "precisionPressure": "",
            "HCl_pressure": "",
            "signal_wl": "",
            "signal_P": "",
            "seedWavelengthCalculated": "",
            "pump_wl": "",
            "Picoscope": "",
            "highVoltage": "",
            "idler_wl": "",
            "idler_wlc": "",
            "simulation": "1,r",
        },
        "y_key": "time",
    },
]


if __name__ == "__main__":
    with DataLoggerDirector(name="call" + name, actor=name) as d:
        # print(d.save_data())
        print(d.ask_rpc(method="set_configuration", configuration=gui_configuration))
        print(d.start_collecting(**configuration))
        print(d.ask_rpc(method="set_plot_configuration", plot_configuration=plot_configuration))

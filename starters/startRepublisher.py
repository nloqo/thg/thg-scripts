from devices.directors import StarterDirector

with StarterDirector(actor="starter") as c:
    c.restart_tasks(("republisher",))

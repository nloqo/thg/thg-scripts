# -*- coding: utf-8 -*-
"""
Read the temperature of the OPO temperature controller arduino. "opoControl"

Created on Thu Dec 15 13:54:36 2022

@author: moneke
"""

import logging
import os
import sys
from time import sleep
import pyvisa

from simple_pid import PID
from pyleco.actors.actor import Actor, DataPublisher
try:
    from pymeasure.instruments.resources import find_serial_port
except ImportError:
    from devices.findDevices import find_serial_port

from devices.arduino import ControllerArduino
from devices.tdk_ntc import ntc_sh

import time

from pymeasure.instruments import Instrument
from pymeasure.adapters import VISAAdapter

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())

rm = pyvisa.ResourceManager("@ivi")
dp832 = rm.open_resource('USB0::0x1AB1::0x0E11::DP8C250200037::INSTR')

# dp832.timeout = 5000


def set_voltage(channel, voltage):
    dp832.write(f":SOUR{channel}:VOLT {voltage}")


def set_current(channel, current):
    dp832.write(f":SOUR{channel}:CURR {current}")


def get_voltage(channel):
    return dp832.query_ascii_values(f":MEAS:VOLT? CH{channel}")[0]


def get_current(channel):
    return dp832.query_ascii_values(f":MEAS:CURR? CH{channel}")[0]


def get_power(channel):
    return get_voltage(channel) * get_current(channel)


def turn_on(channel):
    dp832.write(f":OUTP CH{str(channel)},ON")


def turn_off(channel):
    dp832.write(f":OUTP CH{str(channel)},OFF")


def set_heater_power(channel, current):
    set_current(channel, current)


def initializePowersupply():
    set_voltage(1, 7.6)
    set_voltage(2, 7.6)

    turn_on(1)
    turn_on(2)

# bei 35 °C gut
# Kp_1 = 0.3375
# Ki_1 = 0.005
# Kd_1 = 1.607

# Kp_2 = 0.3375
# Ki_2 = 0.005
# Kd_2 = 1.607

# Kp=1 oszilliert mit Periode von 30s
# Kp=0.5 oszilliert nicht
# Kp=0.75 oszilliert ganz schwach, Periode von 30s
# --> Ziegler Nichols: Kp=0.45, Ki=0.03, Kd=1.69

# bei 50 °C relativ gut
# Kp_1 = 0.1
# Ki_1 = 0.002
# Kd_1 = 0.001

# Kp_2 = 0.2
# Ki_2 = 0.002
# Kd_2 = 0.001

# bei 70 °C und angeklebten Temperatursensoren gut
# Kp_1 = 0.2
# Ki_1 = 0.002
# Kd_1 = 0.0005

# Kp_2 = 0.2
# Ki_2 = 0.002
# Kd_2 = 0.0005


Kp_1 = 0.2
Ki_1 = 0.01
Kd_1 = 0.001

Kp_2 = 0.2
Ki_2 = 0.01
Kd_2 = 0.001

# Kp_2 = 0.2
# Ki_2 = 0.005
# Kd_2 = 0.001


target_temperature = 70.0
compare_resistor = 1.2e3  # 1.2e3 or 1e4

initializePowersupply()

pid_xtal1 = PID(Kp_1, Ki_1, Kd_1, setpoint=target_temperature, starting_output=0.85)
pid_xtal1.output_limits = (0, 1.5)
pid_xtal1.proportional_on_measurement = False
pid_xtal1.differential_on_measurement = False

pid_xtal2 = PID(Kp_2, Ki_2, Kd_2, setpoint=target_temperature, starting_output=0.78)
pid_xtal2.output_limits = (0, 1.5)
pid_xtal2.proportional_on_measurement = False
pid_xtal2.differential_on_measurement = False

# Parameters
# hw_info = 0x2341, 0x0043, "74937303736351E09130"  # Uno
hw_info = 0x2341, 0x0266, None  # Giga
names = ["nsOPOxtal", "nsOPOxtal2", "nsOPOoutside"]  # to publish under
interval = 0.2  # seconds
read_integral: bool = True
integral_name = "nsOPOxtalIntegral"
# Start values for integral and setpoint after reconnecting
integral_start_value: float | None = None  # start integral_value
setpoint: float | None = None  # start setpoint in internal units (higher = colder)
# None uses value stored in EEPROM (currently: 385)
# Temperature in °C: internal value
# 35.45: 400
# 36.0: 394.5
# 36.45: 390
# 36.5: 389.5
# 36.961: 385


converter = ntc_sh.NTC_SH(((10, 19900), (30, 8057), (60, 2488)))
if read_integral:
    names = [integral_name] + names


# -*- coding: utf-8 -*-
"""
Communication with Arduino.

ReadSensors.ino is the corresponding script.

Created on Wed May  4 17:11:43 2022 by Benedikt Burger
"""

class Arduino_16bit(Instrument):
    """
    Communication with an Arduino.

    .. note::

        Connecting to an Arduino resets it. Afterwards it takes a few seconds
        to restart, before it is ready to communicate. Therefore, the init
        waits some time.

    :param adapter: A pyVISA resource_string
    :param str visa: Visa library to use. @py for pyvisa-py.
    """

    def __init__(self, adapter, timeout=1000, startup_sleep=2, **kwargs):
        """Initialize the communication."""
        super().__init__(
            adapter,
            "Arduino Communicator",
            includeSCPI=False,
            write_termination="\n",
            read_termination="\r\n",
            timeout=timeout,
            **kwargs,
        )
        self.resource = adapter
        self.kwargs = kwargs
        """
        Arduino uses the same defaults as visa:
            baudrate 9600
            8 data bits
            no parity
            one stop bit
        """
        time.sleep(startup_sleep)

    def open(self):
        """Open a connection to the Arduino."""
        self.adapter = VISAAdapter(
            self.resource,
            write_termination="\n",
            read_termination="\r\n",
            timeout=1000,
            **self.kwargs,
        )
        time.sleep(2)

    def close(self):
        """Close the connection."""
        self.adapter.close()

    def ping(self):
        """Ping the Arduino."""
        return self.ask("p")

    data = Instrument.measurement(
        "l",
        "List of the latest available data as relative voltage.",
        separator="\t",
        get_process=lambda v: [f / 2**16 for f in v],
    )

    data_averaged = Instrument.measurement(
        "r",
        "List of the data as rolling average, as relative voltage.",
        separator="\t",
        get_process=lambda v: [f / 2**16 for f in v],
    )

    data_raw = Instrument.measurement(
        "q", "List of data with raw values from Arduino.", separator="\t"
    )

    version = Instrument.measurement("v", "Get current firmware version.")

    precision = Instrument.setting(
        "f%i", "Set the precision of the data to be returned."
    )

    averaging = Instrument.setting("m%i", "Set cycles to average over.")


class ControllerArduino_16bit(Arduino_16bit):
    """An Arduino with a temperature controller.

    According to TemperatureController.ino 1.3.0
    """

    def check_set_errors(self):
        got = self.read()
        if got.startswith("ACK"):
            return []
        else:
            raise ConnectionError(f"Received non Acknowledgement: '{got}'.")

    factor_p = Instrument.setting(
        "kp%f",
        """Set the p factor of the PID controller.""",
        check_set_errors=True,
    )

    factor_i = Instrument.setting(
        "ki%f",
        """Set the p factor of the PID controller.""",
        check_set_errors=True,
    )

    # factor_d = Instrument.setting(
    #      "kd%f",
    #      """Set the p factor of the PID controller.""",
    #      check_set_errors=True,
    # )

    setpoint = Instrument.setting(
        "ks%f",
        """Set the setpoint.""",
        check_set_errors=True,
    )

    integral_value = Instrument.setting(
        "kx%f",
        """Set the current integral value of the PID controller.""",
        check_set_errors=True,
    )

    control_enabled = Instrument.setting(
        "o%i",
        """Set whether the control is enabled or not.""",
        values=[False, True],
        map_values=True,
        check_set_errors=True,
    )

    def get_pid_values(self):
        """Get the PID values.

        :return list: integral_value, Kp, Ki, setpoint, duty_cycle
        """
        values = self.values("d", separator=",", cast=str)  # type: ignore
        for i, value in enumerate(values):
            values[i] = float(value.split(":")[-1])
        if len(values) < 5:
            values.append(None)
        return values

    def write_to_EEPROM(self):
        """Write current values to EEPROM."""
        self.write("e")
        self.check_set_errors()

    def get_compilation_parameters(self):
        """Get the compilation parameters (for version>=1.3.0)"""
        values = self.values("x", separator=",", cast=str)  # type: ignore
        d = {}
        for value in values:
            k, v = value.split(":")
            d[k] = v
        return d



class TemperatureControllerArduino(ControllerArduino_16bit):
    """Control the temperature"""

    def readout_temperatures(self):
        """Read the Arduino values and return temperatures in °C."""
        try:
            # Get relative volts.
            volts = self.data
            log.debug(volts)
        except Exception as exc:
            log.exception("Readout failed.", exc_info=exc)
            return []
        try:
            values = [converter.temperature(compare_resistor * v / (1 - v)) for v in volts[:3]]
        except ValueError as exc:
            log.exception("Value error on temperature calculation.", exc)
            return []
        # originally for humidity sensor
        # try:
        #     values.append(1064 * volts[7])
        # except IndexError:
        #     pass  # not enough data received
        return values

    @property
    def temperatures(self) -> list[float]:
        return self.readout_temperatures()


def read_publish(device: TemperatureControllerArduino, publisher: DataPublisher):
    if read_integral:
        values = [device.get_pid_values()[0]]
    else:
        values = []
    values.extend(device.temperatures)
    log.debug(device.temperatures)
    data = {}
    for i in range(min(len(names), len(values))):
        if names[i]:
            data[names[i]] = values[i]

    # add the heating power to the published data
    data["powerXtal1"] = get_power(1)
    data["powerXtal2"] = get_power(2)

    log.debug(data)

    try:
        # print(data)
        # print(data["nsOPOxtal"])

        current_temperature_xtal1 = data["nsOPOxtal"]
        heater_power_xtal1 = pid_xtal1(current_temperature_xtal1)
        set_heater_power(1, heater_power_xtal1)

        current_temperature_xtal2 = data["nsOPOxtal2"]
        heater_power_xtal2 = pid_xtal2(current_temperature_xtal2)
        set_heater_power(2, heater_power_xtal2)

        publisher.send_legacy(data)
    except Exception as exc:
        log.exception("Publisher error.", exc_info=exc)


def task(stop_event):
    with Actor("nsOpoControl", TemperatureControllerArduino, periodic_reading=interval) as actor:
        actor.read_publish = read_publish
        port = find_serial_port(*hw_info)
        actor.connect(adapter=port, visa_library="@py")
        if setpoint is not None:
            actor.device.setpoint = setpoint
        if integral_start_value is not None:
            actor.device.integral_value = integral_start_value
        # Loop
        actor.listen(stop_event=stop_event)
        # Finish (in exit)


if __name__ == "__main__":
    # Run the task if executed.
    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())
    except ValueError as e:
        log.error(f'ValueError: {e}')
    except pyvisa.errors.VisaIOError as e:
        log.error(f'VisaIOError: {e.description}')
    except KeyboardInterrupt:
        pass
    finally:
        turn_off(1)
        turn_off(2)

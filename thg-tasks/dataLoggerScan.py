# -*- coding: utf-8 -*-
"""
Datalogger for scans. 'dataLoggerScan'
"""

import logging

from pyleco.management.data_logger import DataLogger


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
interval = 0.05  # Readout interval in s
start_data = None  # optionally to start measurement at the beginning.


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    dataLogger = DataLogger(name="DataLoggerScan", directory=r"D:\Measurement Data\DataLogger")

    # Continuous loop
    dataLogger.listen(stop_event,
                      start_data=start_data)


if __name__ == "__main__":
    """Run the task if the module is executed."""
    log.addHandler(logging.StreamHandler())
    log.setLevel(logging.INFO)

    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())
    except KeyboardInterrupt:
        pass

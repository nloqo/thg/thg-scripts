# -*- coding: utf-8 -*-
"""
Polarizer attenuator motor controller, communicate as 'polarizer'.
"""


import logging

from pyleco_extras.actors.tmc_motor_actor import TMCMotorActor

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
configs = [
    {'motorNumber': 0,  # OPA 2
     'maxCurrent': 50,
     'standbyCurrent': 5,
     'positioningSpeed': 1000,
     'acceleration': 512,
     'stepResolution': 3,
     'stepCount': 200,
     'unitSize': 0.100603571,
     'unitSymbol': "°",
     'unitOffset': 45,
     'stallguardThreshold': 0,
     },
    {'motorNumber': 3,  # OPO
     'maxCurrent': 50,
     'standbyCurrent': 5,
     'positioningSpeed': 1000,
     'acceleration': 512,
     'stepResolution': 3,
     'stepCount': 200,
     'unitSize': 0.100603571,
     'unitSymbol': "°",
     'unitOffset': 0,
     'stallguardThreshold': 0,
     },
    {'motorNumber': 4,  # MIR
     'maxCurrent': 50,
     'standbyCurrent': 5,
     'positioningSpeed': 1000,
     'acceleration': 512,
     'stepResolution': 3,
     'stepCount': 200,
     'unitSize': 0.100603571,
     'unitSymbol': "°",
     'unitOffset': 40,
     'stallguardThreshold': 0,
     },
]
motorDict = {
    'OPA2': 0,
    'OPO': 3,
    'MIR': 4,
}
# Name to communicate with
name = "polarizer"
# Name of the card or COM port
card = "polarizer"


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    try:
        controller = TMCMotorActor(name, card, motorDict, log=log)
    except Exception as exc:
        log.exception(f"Creation of {name} at card {card} failed.", exc_info=exc)
        return
    for config in configs:
        controller.configure_motor(config)

    # Continuous loop
    controller.listen(stop_event)

    # Finish
    controller.disconnect()
    controller.close()


if __name__ == "__main__":
    # Run the task if executed.
    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())
    except KeyboardInterrupt:
        pass

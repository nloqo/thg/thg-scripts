# -*- coding: utf-8 -*-
"""
Controlling the IPG Fiber Amp. "fiberAmp"
"""


import logging
import time

from pymeasure.instruments.ipgphotonics import YAR
from pyleco.actors.actor import Actor
from pyleco.directors.transparent_director import TransparentDirector
try:
    from pyleco.json_utils.errors import RECEIVER_UNKNOWN, JSONRPCError
except ImportError:
    from pyleco.errors import RECEIVER_UNKNOWN
    from jsonrpcobjects.errors import JSONRPCError


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
name = "fiberAmp"
seed_name = "MYRES.koherasBasik"
COM = 4  # number of COM port
interval = 0.05  # Readout interval in s
step_interval = 10  # Interval between two steps in s


class YAR_mod(YAR):
    """Modified class with control over the output power."""

    def __init__(self, *args, communicator=None, **kwargs):
        super().__init__(*args, **kwargs)
        self.power_setpoint = self._power_setpoint
        self.calculate_next_step()
        self._changing = False
        self.powerRange = self.power_range
        self._power_setpoint_values = self.powerRange
        self.director = TransparentDirector(communicator=communicator, actor=seed_name)

    _power_setpoint = YAR.power_setpoint

    def calculate_next_step(self):
        """Calculate the time for the next step."""
        self._nextStep = time.perf_counter() + step_interval

    @property
    def power_setpoint(self):
        """Control output power setpoint in W (protection enabled)."""
        return self._power_target

    @power_setpoint.setter
    def power_setpoint(self, value):
        if value == "Initializing":
            value = 0
        self._power_target = value
        self._changing = True

    def _change_power(self):
        """Change the power in order to reach the target."""
        emission = self.emission_enabled
        current: float = self._power_setpoint if emission else -1  # type: ignore
        target = self.power_setpoint
        if round(current, 3) == round(target, 3):
            self._changing = False
            return
        elif 0 < current or target > 0 and emission:
            sign = -1 if target < current else 1
            diff = abs(target - current)
            setpoint = current + sign * min(diff, 1)
            if setpoint < self.powerRange[0]:
                setpoint = 0
            self._power_setpoint = setpoint
        elif target >= 0 and not emission:
            try:
                seed_emission = self.director.device.emission_enabled  # type: ignore
                if seed_emission:
                    self.emission_enabled = True
                else:
                    self.director.device.emission_enabled = True  # type: ignore
            except JSONRPCError as exc:
                if exc.rpc_error.code == RECEIVER_UNKNOWN.code:
                    self.director.ask_rpc("start_tasks", names=["koherasBasik"],
                                          actor="MYRES.starter")
        elif target < -1 and current == -1:
            try:
                self.director.device.emission_enabled = False  # type: ignore
            except JSONRPCError as exc:
                log.exception("Disabling seed emission failed.", exc_info=exc)
            self._changing = False
        elif current <= 0:
            self.emission_enabled = False
        self.calculate_next_step()


def readout(device, publisher):
    """Reading the device and doing power steps."""
    if device._changing and time.perf_counter() > device._nextStep:
        device._change_power()


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    with Actor(name, YAR_mod, periodic_reading=interval, log=log) as actor:
        actor.read_publish = readout
        actor.connect(f"ASRL{COM}", communicator=actor)

        # Continuous loop
        actor.listen(stop_event)

        # Finish (in exit)


if __name__ == "__main__":
    # Run the task if executed.
    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())
    except KeyboardInterrupt:
        pass

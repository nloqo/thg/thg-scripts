# -*- coding: utf-8 -*-
"""
A task reading regularly an arduino.
"""


import logging

try:
    from pymeasure.instruments.resources import find_serial_port
except ImportError:
    from devices.findDevices import find_serial_port

from devices import arduino
from pyleco.utils.data_publisher import DataPublisher
from pyleco.utils.timers import SignallingTimer

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


def readout(inst):
    """Read the Arduino values."""
    try:
        # Get relative volts.
        volts = inst.data
    except Exception as exc:
        log.exception("Readout failed.", exc_info=exc)
    else:
        return volts


def base_task(stop_event,
              hw_info: tuple[int, int, str],
              interval: float = 0.2,
              names: list[str] | tuple[str, ...] | None = None,
              readout=readout,
              **kwargs
              ):
    """The task which is run by the starter.

    :param stop_event: The starter's stop event.
    :param hw_info: tuple of vid, pid, sn.
    :param interval: Readout interval in s.
    :param names: List of names to publish under.
    :param callable readout: The readout routine, returning a list of values.
    :param \\**kwargs: Arguments for the readout
    """
    # Initialize
    publisher = DataPublisher("arduino", log=log)
    if names is None:
        names = ()
    try:
        port = find_serial_port(*hw_info)
        inst = arduino.Arduino(port, visa_library="@py")
    except Exception as exc:
        log.exception("Connection failed.", exc_info=exc)
        return
    try:
        inst.ping()
    except Exception as exc:
        log.exception("Ping failed.", exc_info=exc)
        return
    try:
        inst.averaging = 50
    except Exception as exc:
        log.exception("Averaging failed.", exc_info=exc)
        return
    t = SignallingTimer(interval)
    t.start()

    while not stop_event.is_set():
        # Waiting on a timer timeout
        if t.signal.wait(1):
            try:
                values = readout(inst, **kwargs)
            except Exception as exc:
                log.exception("Readout failed.", exc_info=exc)
                break
            else:
                data = {}
                for i in range(min(len(names), len(values))):
                    if names[i]:
                        data[names[i]] = values[i]
                log.debug(data)
                try:
                    publisher.send_legacy(data)
                except Exception as exc:
                    log.exception("Publisher error.", exc_info=exc)
    # Finish
    t.cancel()  # Stop timer
    inst.close()


def task(stop_event):
    """The task which is run by the starter."""
    return
    base_task(stop_event, readout)


if __name__ == "__main__":
    # Run the task if executed.
    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())
    except KeyboardInterrupt:
        pass

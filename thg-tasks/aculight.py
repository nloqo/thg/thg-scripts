# -*- coding: utf-8 -*-
"""
Control the Aculight Argos 2400 OPO. 'aculight'
"""


import logging

from pyleco.actors.actor import Actor

from devices.aculight import Argos


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
interval = -1  # Readout interval in s
com = "ASRL5"


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    with Actor("aculight", Argos, periodic_reading=interval) as actor:
        actor.connect(com)

        # Continuous loop
        actor.listen(stop_event=stop_event)

        # Finish
        # in listen and __exit__ included


if __name__ == "__main__":
    """Run the task if the module is executed."""
    log.addHandler(logging.StreamHandler())
    log.setLevel(logging.INFO)

    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())
    except KeyboardInterrupt:
        pass

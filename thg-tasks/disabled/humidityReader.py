# -*- coding: utf-8 -*-
"""
Read the humidity sensors with an arduino

Created on Thu Dec 15 13:54:36 2022

@author: moneke
"""

import logging
import serial

from devices.tdk_ntc import ntc_sh
try:
    from devices.pyleco_addons.arduino_base_task import base_task
except ImportError:
    from .arduino import base_task


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


converter = ntc_sh.NTC_SH(((10, 19900), (30, 8057), (60, 2488)))


def readout(inst):
    """Read the Arduino values."""
    try:
        # Get relative volts.
        data = inst.ask('y').split('\t')
    except serial.SerialException:
        raise
    except Exception as exc:
        log.warning("Readout failed.", exc_info=exc)
        return []
    for i, d in enumerate(data):
        try:
            data[i] = float(d)
        except Exception as exc:
            log.exception("Data conversion failed", exc_info=exc)
    return data


def task(stop_event):
    base_task(stop_event,
              hw_info=(0x2341, 0x003D, "753303039343517022B0"),
              names=("humidityTemp", "humidity"),
              interval=0.2,
              readout=readout,
              )


if __name__ == "__main__":
    # Run the task if executed.
    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())
    except KeyboardInterrupt:
        pass

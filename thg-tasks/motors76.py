# -*- coding: utf-8 -*-
"""
Control motor card with address 76. 'motors76'
"""


import logging

from pyleco_extras.actors.tmc_motor_actor import TMCMotorActor

log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
configs = [
    {'motorNumber': 3,  # DSUB9  Beamprofiler M1
     'maxCurrent': 50,
     'standbyCurrent': 5,
     'positioningSpeed': 512,
     'acceleration': 512,
     'stepResolution': 3,
     'stepCount': 200,
     'unitSize': 0.25,
     'unitSymbol': "mm",
     'unitOffset': 0,
     'stallguardThreshold': 0,
     },
    {'motorNumber': 4,  # DSUB9  Beamprofiler1 M1
     'maxCurrent': 70,
     'standbyCurrent': 10,
     'positioningSpeed': 45,
     'acceleration': 100,
     'stepResolution': 3,
     'stepCount': 200,
     'unitSize': 0.5,
     'unitSymbol': "mm",
     'unitOffset': 0,
     'stallguardThreshold': 0,
     },
    {'motorNumber': 5,  # DSUB15 Beamprofiler1 M0
     'maxCurrent': 50,
     'standbyCurrent': 5,
     'positioningSpeed': 512,
     'acceleration': 512,
     'stepResolution': 3,
     'stepCount': 200,
     'unitSize': 0.25,
     'unitSymbol': "mm",
     'unitOffset': 0,
     'stallguardThreshold': 0,
     },
]
motorDict = {
    'BeamprofilerM1': 4,
    'BeamprofilerM2': 3,
    'Beamprofiler1M0': 5,
    'Beamprofiler1M1': 4,
}
name = "motors76"
card = "Beamprofiler"


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    try:
        controller = TMCMotorActor(name, card, motorDict, log=log)
    except Exception as exc:
        log.exception(f"Creation of {name} at card {card} failed.", exc_info=exc)
        return
    for config in configs:
        controller.configure_motor(config)

    # Continuous loop
    controller.listen(stop_event)

    # Finish
    controller.disconnect()
    controller.close()


if __name__ == "__main__":
    # Run the task if executed.
    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())
    except KeyboardInterrupt:
        pass

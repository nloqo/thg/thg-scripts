# -*- coding: utf-8 -*-
"""
OPA1 oven. "OPA1oven"
"""


import logging

from pymeasure.instruments.hcp.tc038 import TC038
try:
    from pymeasure.instruments.resources import find_serial_port
except ImportError:
    from devices.findDevices import find_serial_port
import pyvisa as pv

from pyleco.actors.actor import Actor


log = logging.getLogger(__name__)
log.addHandler(logging.NullHandler())


# Parameters
interval = 1


class TC038_mod(TC038):

    def ask(self, command, **kwargs):
        for _ in range(3):
            try:
                value = super().ask(command, **kwargs)
            except pv.errors.VisaIOError as exc:
                if exc.error_code == pv.constants.StatusCode.error_serial_parity:
                    error = "parity error"
                elif exc.error_code == pv.constants.StatusCode.error_serial_framing:
                    error = "framing error"
                elif exc.error_code == pv.constants.StatusCode.error_serial_overrun:
                    error = "overrun error"
                else:
                    log.warning(f"Communication error {exc}")
                    self.adapter.connection.clear()
                    continue
                log.warning(f"VISA error {error}")
            else:
                return value
        raise ConnectionAbortedError("Reading failed after 3 tries.")


def readout(device, publisher):
    publisher.send_legacy({'OPA1oven': device.temperature})


def task(stop_event):
    """The task which is run by the starter."""
    # Initialize
    with Actor("OPA1oven", TC038_mod, periodic_reading=interval, log=log) as c:
        c.read_publish = readout
        c.connect(find_serial_port(4292, 60000, "1610207"))

        # Loop
        c.listen(stop_event)

        # Close (in exit)


if __name__ == "__main__":
    # Run the task if executed.
    class Signal:
        def is_set(self):
            return False
    try:
        task(Signal())
    except KeyboardInterrupt:
        pass

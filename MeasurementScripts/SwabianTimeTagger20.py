# -*- coding: utf-8 -*-
"""
Created on Tue Aug  9 11:38:53 2022

@author: THG-User
"""

from time import sleep

from devices import intercom
import TimeTagger as tt

interval = 100  # ms


if __name__ == "__main__":
    publisher = intercom.Publisher()
    tagger = tt.createTimeTagger()
    rate = tt.Counter(tagger, [1], binwidth=interval/1000*1e12, n_values=1)
    rate.start()
    try:
        while True:
            publisher({'tt20': rate.getData()[-1]/(interval/1000)})
            sleep(interval / 1000)
    except KeyboardInterrupt:
        tt.freeTimeTagger(tagger)
        pass

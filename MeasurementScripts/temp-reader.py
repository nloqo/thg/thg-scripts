# -*- coding: utf-8 -*-
"""
Created on Tue Jul  5 15:31:08 2022

@author: THG-User
"""

# %% Imports
import time

from devices import intercom
import nidaqmx
from devices.tdk_ntc import ntc_sh
import numpy as np


# %% Setup
converter = ntc_sh.NTC_SH(((10, 19900), (30, 8057), (60, 2488)))

publisher = intercom.Publisher()

niTask = nidaqmx.Task()
niTask.ai_channels.add_ai_voltage_chan("Dev1/ai3")


# %% Execute
while True:
    try:
        volt = np.average(niTask.read(number_of_samples_per_channel=5)) / 5
        temp = converter.temperature(1e4 * volt / (1-volt))
        publisher({'OPOBlockT2': temp})
        time.sleep(0.2)
    except KeyboardInterrupt:
        break


# %% Finish
niTask.close()

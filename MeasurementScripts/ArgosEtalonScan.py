# -*- coding: utf-8 -*-
"""
Created on Tue Jul  5 15:31:08 2022

@author: THG-User
"""

# %% Imports
import time

from devices import Aculight, intercom
import numpy as np


# %% Setup
opo = Aculight.Argos(5)
publisher = intercom.Publisher()


# %% Execute
opo.setEtalon(-4)
time.sleep(2)
for angle in np.arange(-4, 4, 0.01):
    try:
        opo.setEtalon(angle)
        publisher({'etalon': angle})
        time.sleep(1)
    except KeyboardInterrupt:
        break
